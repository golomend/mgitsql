# Organisation du dépôt Cloud

Vous trouverez 6 dossiers regroupant tout le contenu nécessaire à un bon suivi du cours **Concevoir et manipuler des bases de données**. N'hésitez pas à me contacter via <guillaume.mangione@gmail.com> en cas d'anomalie ou d'erreur sur un script SQL, par exemple.  

# administratif
Documents liés aux Cours Municipaux pour Adultes. On peut y trouver un accès à la description du cours indiquée sur le site, le calendrier des cours, les contacts utiles, la fiche pédagogique du site des CMA de Paris...  

# exemple
Espace qui référence des scripts de démonstration de toutes les opérations en SQL que l'on a pu aborder en cours. Il permet de retrouver plus facilement des exemples pour chaque cas.

# cours
Supports de cours au format PDF, permettant d'avoir un récapitulatif du cours donné en présentiel. Vous y trouverez en complément, les notes utiles pour approfondir les contenus.

# partage
Vous pouvez déposer vos travaux ou exercices dans cet espace afin de les partager avec d'autres auditeur·rice·s ou même demander à Guillaume de regarder ce que vous avez fait.  
Attention, cet espace est libre d'écriture, vos travaux n'ont aucune garantie d'être restaurés en cas de suppression ou modification.

# pratique
Espace dédié à la mise en pratique de la modélisation et du SQL. Plusieurs thèmes sont abordés pour obtenir une vision plus large qu'un seul contexte. Les exercices sont plus ou moins complexes. On trouve dans chaque domaine abordé, un dossier `common` qui permet de centraliser les fichiers communs aux différents SGBDR proposés. Pour l'heure, les travaux sont essentiellement réalisés avec MySQL. Mais rien ne vous empêche d'utiliser un autre SGBDR et proposer vos travaux à Guillaume.

# ressources
Ressources qui viennent du Web, il peut s'agir d'articles, de documentations ou même de liens utiles pour proposer des outils ou des compléments d'informations pour les auditeur·rice·s.



# Votre environnement de travail


Pour exploiter au mieux le contenu de ce dépôt sur votre PC/Mac, il est nécessaire d'installer des programmes et extensions. Voici quelques suggestions d'outils à installer :

> `Uwamp`
https://www.uwamp.com/fr  
Outil qui peut vous servir à vous défaire de toute la phase d'installation d'un environnement pour utiliser un SGBDR. Il va vous simuler un serveur Web sur votre propre PC. Une interface Web (en PHP) vous permet même de vous connecter sur MySQL pour effectuer vos requêtes SQL.  
Il existe une version pour MAC/Linux XAMPP, qui s'installe et permet de faire la même chose.

>`Notepad++`
https://notepad-plus-plus.org/downloads/  
C'est un éditeur de code standard qui permet de créer très facilement des scripts dans divers langages informatiques. Il est convivial et offre de nombreuses extensions intéressantes.

>`SublimeText`
https://www.sublimetext.com  
C'est un éditeur de code très simple et facile de prise en main. Il offre de nombreuses fonctionnalités accessibles même pour les néophytes. Vivement conseillé pour les personnes n'ayant jamais travaillées avec des IDE ou éditeurs de code.

>`Visual Studio Code`
https://code.visualstudio.com  
C'est en éditeur de code qui facilite la création de scripts SQL avec une interface conviviale comprenant notamment une coloration syntaxique, une gestion de l'indentation et même l'autocomplétion.

>`SQL Beautify`
https://marketplace.visualstudio.com/items?itemName=sensourceinc.vscode-sql-beautify  
C'est une extension de `Visual Studio Code` qui met en couleur les mots clés SQL dans vos scripts. Cela améliore grandement la lecture de vos scripts SQL.

>`Cmder`
https://cmder.net  
Invite de commande améliorée, permet une meilleure lecture des commandes, on peut par exemple effectuer un split de fenêtres, effectuer des zooms sur le contenu (avec la molette de la souris). On peut intégrer très facilement du `Python` ou des extensions comme `mycli`.

>`pgAdmin`
https://www.pgadmin.org  
Outil d'administration de bases de données `Postgresql`. Il offre une interface Web conviviale dans sa dernière version.


