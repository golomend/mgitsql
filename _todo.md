# TODO

Différents axes d'amélioration du support pédagogique.

# Présentations
- [ ] Les cours sont au format PPT, peut-être fournir un support Web pour l'aspect pédagogique.
- [ ] Une roadmap au format Web serait plus sympa.
- [ ] Intégrer les sources des cours sur un dépôt Git

# Administratif
- [ ] Dynamiser les calendriers des cours (scripts automatique à partir d'une matrice de dates)

# Pratique
Fournir un tableau récapitulant tous les exercices disponibles avec les sujets abordés. Par exemple, la personne souhaite effectuer du SQL avec des agrégats, on lui conseille d'utiliser telles bases de données.
Fournir un support avec `Postgres` progressivement.

# Idées

- Se connecter sur un serveur distant.
- Se connecter en `SSH`.
- Présenter des données de `Postgresql` sur une carte via `Leaflet`, une librairie `JavaScript` pour des cartes interactives.
- Utiliser une `Raspberry` avec un moteur SQL installé et proposer des connexions des auditeurs en `SSH`.
