-- ---------------------------------------------------------------------
-- Moteur : PostgreSQL
-- Auteur : Guillaume Mangione
-- Date de création : 10/2019
-- Base de données : security_db
--
-- Permet de gérer la sécurité des accès aux applications d'une organisation
-- en fonction des profils utilisateurs.
-- On trouve également une gestion d'historique des attributions de droits.
-- ---------------------------------------------------------------------

DO
$do$
BEGIN
   IF NOT EXISTS (SELECT * FROM pg_database WHERE datname = 'disquaire') THEN
	   CREATE DATABASE disquaire;
   END IF;
END
$do$;

