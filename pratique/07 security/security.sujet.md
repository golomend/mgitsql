# Security
#### DIFFICULTÉ : 4 / 5

## Contexte
Vous êtes en charge de modéliser un Système d'Information (SI) permettant de gérer les différentes autorisations d'accès aux applications pour l'ensemble des employés de notre organisation. Plusieurs applications vont devoir s'appuyer sur votre SI afin d'autoriser ou non certaines fonctionnalités.

## Explications
Chaque employé, lorsqu'il utilise une de nos applications, effectue une authentification qui lui est propre. Cela lui permettra d'une part, d'utiliser ou non l'application et d'autre part d'accéder ou non à certaines fonctionnalités en fonction du profil qui lui a été attribué. Au moment de l'authentification, l'employé va donc fournir son nom de session, son mot de passe ainsi que le profil avec lequel il souhaite utiliser l'application pour qu'il soit identifié.  
Chaque employé peut disposer de plusieurs profils, selon son profil, il aura accès à certaines fonctionnalités des différentes applications. Le profil Super Administrateur a accès à l'ensemble des fonctionnalités de toutes les applications.  
*Exemple : Un médecin a droit à toutes les fonctionnalités de l'application `Régulation Médicale` mais seulement certaines fonctionnalités d'`Opérations`.*

Voici la liste des applications ainsi que leurs fonctionnalités :  

`Appels`
  - A01 - historique d'appels
  - A02 - suivi d'activité en temps réel

`Opérations`
  - O01 - renforts
  - 002 - contrôleur d'événements
  - O03 - accès main courante
  - O04 - suivi des opérations

`Régulation Médicale`
  - R01 - visualisation d'un bilan
  - R02 - envoi d'un renfort médical
  - R03 - recherche d'un patient
  - R04 - statistiques
  - R05 - suivi des AR
  - R06 - suivi patient

`Supervision`
  - S01 - gestion des utilisateurs
  - S02 - gestion des profils
  - S03 - gestion des droits utilisateur
  - S04 - réactivation d'un utilisateur
  - S05 - état des connexions en cours
  - S06 - cartographie opérationnelle

`Paramétrage`
  - P01 - gestion des codes motifs d'appels
  - P02 - gestion des adresses
  - P03 - gestion des moyens
  - P04 - gestion des règles opérationnelles

`Moyens`
  - M01 - accès moyens niveau 1
  - M02 - accès moyens niveau 2
  - M03 - accès moyens niveau 3
  - M04 - gestion d'un moyen
  - M05 - gestion des disponibilités
  - M06 - gestion des statuts

Et voici la liste des profils :
 - `ADM` : Administrateur
 - `PAR` : Paramétreur
 - `OPE` : Opérateur
 - `REG` : Régulateur
 - `MED` : Médecin
 - `IDE` : Infirmier
 - `STA` : Stationnaire
 - `CTR` : Contrôleur

Quelques exemples de droits déjà rédigés, la liste n'est pas exhaustive :  
**Guillaume MANGIONE (mangioneg) -code : GM78794**
- Administrateur (toutes les fonctions sauf G03, G04 et O03).

**Martin Hill (hillm) - code : MH95889**
- Contrôleur (M02, M04, A01, A02, O01-O04)
- Opérateur (M01, M04, M05, O01, O03, O04, A01, A02)

**Olivier Stibout (stibouto) - code : OS97865**
- Médecin (R01-R06, M01-M03, M05, S06, O03 et O04)

**Yann Frein (freiny) - code : YF74553**
- Opérateur (M01, M04, M05, O01, O03, O04, A01, A02)
- Paramétreur (P01-P04, M01-M04, S01-S06, O01-O04)


## Règles de gestion
`RG01:` Attribuer des droits sur une fonctionnalité amène automatiquement une possibilité de connexion vers l'application qui détient cette fonctionnalité.  
`RG02:` Un nom de session employé est composé du nom de famille ainsi que de la 1ère lettre du prénom. En cas d'homonyme, on rajoute la seconde lettre du prénom, puis la 3ème, ainsi de suite. Si l'homonyme se porte et sur le nom et sur le prénom, on ajoutera le chiffre '2' au prenom de session du dernier employé.  
`RG03:` On doit connaître pour un employés, son nom, son prénom et son adresse mail. Il faut pouvoir savoir si l'employé est encore présent dans l'organisation.  
`RG04:` Quand on attribue des droits d'accès à une fonctionnalité, il faut savoir quand (GDH) et par qui cette action d'attribution a été réalisée. Il faut également savoir qui a retiré les droits d'accès à une fonctionnalité. (nous avons les actions d'attribution et de retrait).  
`RG05` Si un employé se trompe 3 fois lors de sa phase d'authentification, son compte est verrouillé.  
`RG06` On ne peut pas avoir 2 fois le même code profil.  
`RG07` Chaque employé dispose d'un code unique (constitué de ses initiales prénom et nom).  
`RG08` Chaque fonctionnalité est distinct des autre par son code.  
`RG09` Les fonctionnalités sont liées à un profil et non à un employé. Cela veut dire que la modification d'un profil entraîne une utilisation de fonctionnalité pour tous les employés ayant ce profil.  

## Premier travail
- Réaliser un MCD à partir de l’énoncé ci-dessus.
- Réaliser le modèle relationnel (MLD) à partir du MCD précédent.


## Deuxième travail
- Proposer un dictionnaire de données.\
  Il faut indiquer le nom de l’attribut (nom technique), son type et sa taille éventuelle, une description éventuelle. Pour chaque attribut, il faut préciser l’entité.
- Réaliser un MPD à l'aide du MCD précédemment créé et de l’énoncé ci-dessus.\


## Troisième travail
- Construire la structure de la base de données à l'aide du MPD en respectant les contraintes indiquées. Veillez à garder la structure dans un fichier `.sql`
- Faire de même avec l'alimentation des données dans la base en suivant les données déjà disponibles dans l'énoncé.


## Livrables attendus
Vos travaux pour le MCD peuvent se représenter via une image (png, jpg), vous enregistrez votre fichier sous cette forme : `[votre_nom]_security.mcd.[ext]`.  
Et pour le MLD, vous pouvez le réaliser avec un simple fichier .txt ou .doc sous la même forme, tel que `[votre_nom]_security.mld.[ext]`.  
Vous déposerez ce fichier dans le partage de Dropbox ou Drive.
