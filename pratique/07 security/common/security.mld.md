# Security : Modèle Logique de Données


`employes` (**<u>idEmploye</u>**, nomSession, motDePasse, nom, prenom, email, code, actif, verrou)  
`applications` (**<u>idApplication</u>**, application)  
`fonctionnalites` (**<u>idFonctionnalite</u>**, code, fonctionnalite, #idApplicationRef)  
`profils` (**<u>idProfil</u>**, codeProfil, profil)  
`disposer` (**#<u>idProfilDisposer</u>**, **#<u>idUtilisateurDisposer</u>**)  
`attribuer` (**<u>idAttribution</u>**, #idAttributeur, #idProfilCible, #idFonctionnaliteCible, dateAction, typeAction)  


**<u>PK</u>** : _(souligné en gras)_  
#FK : _(préfixé d'un dièse (#))_