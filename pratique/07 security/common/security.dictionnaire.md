# Security : Dictionnaire de données


## `employes`
Un employé est une personne connue de l'organisation et peut bénéficier d'accès aux applications présentes sur le réseau.

| colonne          | type                      | contraintes             | description                                                           |
|------------------|---------------------------|-------------------------|-----------------------------------------------------------------------|
| `idEmploye`      | ENTIER                    | ID, NOT NULL            | Identifiant unique.                                                   |
| `nomSession`     | CHAÎNE DE CARACTÈRE (50)  | UNIQUE, NOT NULL        | Nom d'employé + première lettre du prénom                             |
|                  |                           |                         | En cas d'homonymie, on ajoute la 2nde lettre du prénom, puis la 3ème. |
| `motDePasse`     | CHAÎNE DE CARACTÈRE (32)  | NOT NULL                | Hash du mot de passe en SHA-1.                                        |
| `nom`            | CHAÎNE DE CARACTÈRE (40)  | NOT NULL                | Nom de l'employé.                                                     |
| `prenom`         | CHAÎNE DE CARACTÈRE (40)  | NOT NULL                | Prénom de l'employé.                                                  |
| `email`          | CHAÎNE DE CARACTÈRE (255) | UNIQUE, NOT NULL        | Adresse mail professionnelle de l'employé.                            |
| `code`           | CHAÎNE DE CARACTÈRE (8)   | UNIQUE, NOT NULL        | Code d'identification unique d'un employé.                            |
| `actif`          | BOOLÉEN                   | DEFAULT TRUE, NOT NULL  | L'employé est-il encore dans l'organisation.                          |
| `verrou`         | BOOLÉEN                   | DEFAULT FALSE, NOT NULL | Permet de savoir si l'employé a son compte de verrouillé.             |


## `applications`
Une application regroupe un ensemble de `fonctionnalités` représentant des processus métier de l'organisation. Les employés peuvent les utiliser en s'identifiant.

| colonne          | type                      | contraintes             | description             |
|------------------|---------------------------|-------------------------|-------------------------|
| `idApplication`  | ENTIER                    | ID, NOT NULL            | Identifiant unique.     |
| `application`    | CHAÎNE DE CARACTÈRE (50)  | UNIQUE, NOT NULL        | Nom de l'application.   |


## `fonctionnalites`
Une fonctionnalité permet de représenter une tâche, un processus ou une action répétitive d'un employé dans l'organisation. Elles sont inscrites dans les applications selon une structure particulière. Chaque fonctionnalité nécessite des droits pour pouvoir y accéder. Ces droits sont liés aux `profils`.

| colonne            | type                      | contraintes             | description                                                                  |
|--------------------|---------------------------|-------------------------|------------------------------------------------------------------------------|
| `idFonctionnalite` | ENTIER                    | ID, NOT NULL            | Identifiant unique.                                                          |
| `code`             | CHAÎNE DE CARACTÈRE (5)   | UNIQUE, NOT NULL        | Code de la fonctionnalité,                                                   |
|                    |                           |                         | composé de la première lettre de l'application + incrément sur 2 chiffres.   |
| `fonctionnalite`   | CHAÎNE DE CARACTÈRE (50)  | UNIQUE, NOT NULL        | Nom de la fonctionnalité.                                                    |
| `idApplicationRef` | ENTIER                    | FK, NOT NULL            | Référence de l'application pour la fonctionnalité.                           |


## `profils`
Un profil permet de déterminer comment l'`employe` sera reconnu par les `applications` de l'organisation. Selon le `profil`, on détient des accès à des `fonctionnalités` aux `applications`.

| colonne          | type                       | contraintes             | description                        |
|------------------|----------------------------|-------------------------|------------------------------------|
| `idProfil`       | ENTIER                     | ID, NOT NULL            | Identifiant unique.                |
| `codeProfil`     | CHAÎNE DE CARACTÈRE (3)    | UNIQUE, NOT NULL        | Code du profil sur 3 caractères.   |
| `profil`         | CHAÎNE DE CARACTÈRE (100)  | NOT NULL                | Intitulé du profil.                |


## `disposer`
Permet de savoir quels sont les `profils` qu'un `employe` possède. Un `employe` peut bénéficier de plusieurs `profils` et à chaque fois qu'il s'identifie sur une `application`, il doit renseigner avec quel `profil` il veut l'utiliser.

| colonne                 | type       | contraintes             | description                                      |
|-------------------------|------------|-------------------------|--------------------------------------------------|
| `idProfilDisposer`      | ENTIER     | ID, FK, NOT NULL        | Identifiant unique et référence le profil.       |
| `idUEmployeDisposer`    | ENTIER     | ID, FK, NOT NULL        | Identifiant unique et référence l'employé.       |


## `attribuer`
Permet de garder un historique des attributions de droits d'accès aux fonctionnalités pour chaque `profil`. Ainsi on sait qui a donné quel droit à une fonctionnalité pour un profil donné. Il en est de même pour le retrait des droits aux fonctionnalités. Cela constitue une trace d'attribution des affectations de droits.

| colonne                  | type                      | contraintes             | description                                                        |
|--------------------------|---------------------------|-------------------------|--------------------------------------------------------------------|
| `idAttribution`          | ENTIER                    | ID, NOT NULL            | Identifiant unique.                                                |
| `idAttributeur`          | ENTIER                    | FK, NOT NULL            | Référence l'employé qui effectue un changement de droit.           |
| `idProfilCible`          | ENTIER                    | FK, NOT NULL            | Référence le profil concerné par le changement de droit.           |
| `idFonctionnaliteCible`  | ENTIER                    | FK, NOT NULL            | Référence l'application concernée par le changement de droit.      |
| `dateAction`             | DATE ET HEURE             | NOT NULL                | Date et heure exacte de l'application du changement de droit.      |
| `typeAction`             | CHAÎNE DE CARACTERES(20)  | NOT NULL                | On peut 'retirer' ou 'appliquer' une fonctionnalité à un profil.   |