-- ---------------------------------------------------------------------
-- Moteur : SQL Server
-- Auteur : Guillaume Mangione
-- Date de création : 10/2019
-- Base de données : security_db
--
-- Permet de gérer la sécurité des accès aux applications d'une organisation
-- en fonction des profils utilisateurs.
-- On trouve également une gestion d'historique des attributions de droits.
-- ---------------------------------------------------------------------

-- La base de données master contient l’intégralité des informations système.
USE [master];



-- ---------------------------------------------------------------------
-- Réinitialisation (ou création) de la base de données et des schémas.

IF NOT EXISTS (SELECT name FROM dbo.sysdatabases WHERE name = N'security_db')
CREATE DATABASE [security_db];
GO

USE [security_db];



-- ---------------------------------------------------------------------
-- Suppression des tables si existantes.

IF OBJECT_ID('dbo.posseder', 'U') IS NOT NULL
    DROP TABLE dbo.posseder;
GO

IF OBJECT_ID('dbo.attribuer', 'U') IS NOT NULL
    DROP TABLE dbo.attribuer;
GO

IF OBJECT_ID('dbo.fonctionnalites', 'U') IS NOT NULL
    DROP TABLE dbo.fonctionnalites;
GO

IF OBJECT_ID('dbo.applications', 'U') IS NOT NULL
    DROP TABLE dbo.applications;
GO

IF OBJECT_ID('dbo.utilisateurs', 'U') IS NOT NULL
    DROP TABLE dbo.utilisateurs;
GO

IF OBJECT_ID('dbo.profils', 'U') IS NOT NULL
    DROP TABLE dbo.profils;
GO


-- ---------------------------------------------------------------------
-- Création de la structure des tables.

CREATE TABLE dbo.utilisateurs
(
   idUtilisateur           INTEGER           NOT NULL PRIMARY KEY,
   session                 VARCHAR(50)       NOT NULL UNIQUE,
   motDePasse              VARCHAR(40)       NOT NULL,
   nom                     VARCHAR(40)       NOT NULL,
   prenom                  VARCHAR(40)       NOT NULL,
   email                   VARCHAR(255)      NOT NULL UNIQUE,
   code                    CHAR(8)           NOT NULL UNIQUE,
   actif                   BIT               NOT NULL DEFAULT 1,
   verrou                  BIT               NOT NULL DEFAULT 0
);

CREATE TABLE dbo.profils
(
   idProfil                INTEGER           NOT NULL PRIMARY KEY,
   codeProfil              CHAR(3)           NOT NULL UNIQUE,
   profil                  VARCHAR(100)      NOT NULL
);

CREATE TABLE dbo.applications
(
   idApplication           INTEGER           NOT NULL PRIMARY KEY,
   application             VARCHAR(50)       NOT NULL
);

CREATE TABLE dbo.fonctionnalites
(
   idFonctionnalite        INTEGER           NOT NULL PRIMARY KEY,
   code                    CHAR(5)           NOT NULL UNIQUE,
   fonctionnalite          VARCHAR(50)       NOT NULL,
   idApplicationRef        INTEGER           NOT NULL,
   CONSTRAINT fk_fonctionnalite_application FOREIGN KEY (idApplicationRef) REFERENCES applications (idApplication)
);

CREATE TABLE dbo.posseder
(
   idProfilPosseder        INTEGER           NOT NULL,
   idUtilisateurPosseder   INTEGER           NOT NULL,
   CONSTRAINT pk_posseder PRIMARY KEY (idProfilPosseder, idUtilisateurPosseder),
   CONSTRAINT fk_profil_posseder FOREIGN KEY (idProfilPosseder) REFERENCES profils (idProfil),
   CONSTRAINT fk_utilisateur_posseder FOREIGN KEY (idUtilisateurPosseder) REFERENCES utilisateurs (idUtilisateur)
);

CREATE TABLE dbo.attribuer
(
   idAttribution           INTEGER           NOT NULL PRIMARY KEY,
   idAttributeur           INTEGER           NOT NULL,
   idProfilCible           INTEGER           NOT NULL,
   idFonctionnaliteCible   INTEGER           NOT NULL,
   dateAction              DATETIME          NOT NULL DEFAULT GETDATE(),
   typeAction              VARCHAR(10)       NOT NULL DEFAULT 'appliquer',
   CONSTRAINT fk_attributeur FOREIGN KEY (idAttributeur) REFERENCES utilisateurs (idUtilisateur),
   CONSTRAINT fk_profil_cible FOREIGN KEY (idProfilCible) REFERENCES profils (idProfil),
   CONSTRAINT fk_fonctionnalite_cible FOREIGN KEY (idFonctionnaliteCible) REFERENCES fonctionnalites (idFonctionnalite),
   CONSTRAINT chk_type_action CHECK (typeAction IN ('appliquer', 'retirer'))
);


-- ---------------------------------------------------------------------
-- Ajout de quelques données de base.

INSERT INTO applications (idApplication, application)
VALUES  (1, 'Appels'),
        (2, 'Opérations'),
        (3, 'Régulation Médicale'),
        (4, 'Supervision'),
        (5, 'Paramétrage'),
        (6, 'Moyens');

INSERT INTO fonctionnalites (idFonctionnalite, code, fonctionnalite, idApplicationRef)
VALUES  (1, 'A01', 'Historique d''appels', 1),
        (2, 'A02', 'Suivi d''activité en temps réel', 1),
        (3, 'O01', 'Renforts', 2),
        (4, 'O02', 'Contrôleur d''évenements', 2),
        (5, 'O03', 'Accès main courante', 2),
        (6, 'O04', 'Suivi des opérations', 2),
        (7, 'R01', 'Visualisation d''un bilan', 3),
        (8, 'R02', 'Envoi d''un renfort médical', 3),
        (9, 'R03', 'Recherche d''un patient', 3),
        (10, 'R04', 'Statistiques', 3),
        (11, 'R05', 'Suivi des AR', 3),
        (12, 'R06', 'Suivi patient', 3),
        (13, 'S01', 'Gestion des utilisateurs', 4),
        (14, 'S02', 'Gestion des profils', 4),
        (15, 'S03', 'Gestion des droits utilisateur', 4),
        (16, 'S04', 'Réactivation d''un utilisateur', 4),
        (17, 'S05', 'Etat des connexions en cours', 4),
        (18, 'S06', 'Cartographie opérationnelle', 4),
        (19, 'P01', 'Gestion des codes motifs d''appels', 5),
        (20, 'P02', 'Gestion des adresses', 5),
        (21, 'P03', 'Gestion des moyens', 5),
        (22, 'P04', 'Gestion des règles opérationnelles', 5),
        (23, 'M01', 'accès moyens niveau 1', 6),
        (24, 'M02', 'accès moyens niveau 2', 6),
        (25, 'M03', 'accès moyens niveau 3', 6),
        (26, 'M04', 'gestion d''un moyen', 6),
        (27, 'M05', 'gestion des disponibilités', 6),
        (28, 'M06', 'gestion des status', 6);

INSERT INTO profils (idProfil, codeProfil, profil)
VALUES  (1, 'ADM', 'Administrateur'),
        (2, 'PAR', 'Paramétreur'),
        (3, 'OPE', 'Opérateur'),
        (4, 'REG', 'Régulateur'),
        (5, 'MED', 'Médecin'),
        (6, 'IDE', 'Infirmier'),
        (7, 'STA', 'Stationnaire'),
        (8, 'CTR', 'Contrôleur');

INSERT INTO utilisateurs (idUtilisateur, session, motDePasse, nom, prenom, email, code, actif, verrou)
VALUES  (1, 'mangioneg', '85d800c576b360a0ac4259c0ce15ac125f17adc8', 'Mangione', 'Guillaume', 'guillaume.mangione@org.com', 'GM78794', 1, 0),
        (2, 'hillm', '29589141d0019d89d4576e8f4393ca7164ea774b', 'Hill', 'Martin', 'martin.hill@org.com', 'MH95889', 1, 0),
        (3, 'stibouto', '407dc369781d06691f2300890f267340c9ab5e12', 'Stibout', 'Olivier', 'olivier.stibout@org.com', 'OS97865', 1, 0),
        (4, 'freiny', 'd82eb6d01ef1905b3f6bb5ebb71d55c13e5dd8b7', 'Frein', 'Yann', 'yann.frein@org.com', 'YF74553', 0, 1);

INSERT INTO posseder (idProfilPosseder, idUtilisateurPosseder)
VALUES  (1, 1),
        (8, 2),
        (3, 2),
        (5, 3),
        (3, 4),
        (2, 4);

-- ---------------------------------------------------------------------
-- Affichage de la structure des tables.

-- TODO



-- ---------------------------------------------------------------------
-- Affichage des données de base.

-- Applications et leurs fonctionnalités.
SELECT fct.idFonctionnalite, fct.code, fct.fonctionnalite, app.application FROM applications app
INNER JOIN fonctionnalites fct ON fct.idApplicationRef = app.idApplication;

-- Droits utilisateurs.
SELECT session, u.code, actif, verrou, codeProfil, profils.profil FROM posseder p 
INNER JOIN utilisateurs u ON u.idUtilisateur = p.idUtilisateurPosseder
INNER JOIN profils ON profils.idProfil = p.idProfilPosseder;


-- ---------------------------------------------------------------------
-- Fin
-- ---------------------------------------------------------------------
