# Agricole : Dictionnaire de données

## `commune`

C'est une localisation géographique pour les agences comptables. Les exploitations agricoles sont référencées sur une seule commune.

| colonne                     | type                       | contraintes       | description                                                              |
|-----------------------------|----------------------------|-------------------|--------------------------------------------------------------------------|
| `id_commune`                | ENTIER                     | ID, NOT NULL      | Identifiant unique auto généré.                                          |
| `nom_commune`               | CHAÎNE DE CARACTÈRES (250) | NOT NULL          | Nom d'une commune française.                                             |
| `code_postal`               | CHAÎNE DE CARACTÈRES (5)   | NOT NULL          | Code postal lié à la commune.                                            |


## `agence`

Agence comptable qui regroupe plusieurs comptables et un directeur d'agence. L'agence est en charge des exploitations agricoles.

| colonne                     | type                       | contraintes       | description                                                              |
|-----------------------------|----------------------------|-------------------|--------------------------------------------------------------------------|
| `id_agence`                 | ENTIER                     | ID, NOT NULL      | Identifiant unique auto généré.                                          |
| `nom_agence`                | CHAÎNE DE CARACTÈRES (250) | NOT NULL          | Nom donné à l'agence comptable.                                          |
| `directeur`                 | CHAÎNE DE CARACTÈRES (50)  | NOT NULL          | Nom du directeur de l'agence comptable.                                  |
| `id_commune_agence`         | ENTIER                     | NOT NULL          | Référence la commune où se situe l'agence.                               |


## `comptable`

Un comptable est une personne s'occupant d'une ou plusieurs exploitations agricoles. Il est positionné dans une agence.

| colonne                     | type                       | contraintes       | description                                                              |
|-----------------------------|----------------------------|-------------------|--------------------------------------------------------------------------|
| `id_comptable`              | ENTIER                     | ID, NOT NULL      | Identifiant unique auto généré.                                          |
| `nom_comptable`             | CHAÎNE DE CARACTÈRES (50)  | NOT NULL          | Nom de famille du comptable.                                             |
| `prenom_comptable`          | CHAÎNE DE CARACTÈRES (50)  | NOT NULL          | Prénom du comptable.                                                     |
| `date_arrivee`              | DATE                       | NOT NULL          | Date d'arrivée du comptable dans la société.                             |
| `poste_telephonique`        | CHAÎNE DE CARACTÈRES (20)  | NOT NULL          | Numéro de téléphone où on peut joindre le comptable.                     |
| `id_agence_comptable`       | ENTIER                     | NOT NULL          | Référence l'agence du comptable.                                         |


## `exploitation`

Les exploitations agricoles sont situées sur une commune, on peut trouver une exploitation située géographiquement sur plusieurs communes mais nous ne référencerons qu'une seule commune pour situer l'exploitation agricole.

| colonne                     | type                       | contraintes       | description                                                              |
|-----------------------------|----------------------------|-------------------|--------------------------------------------------------------------------|
| `id_exploitation`           | ENTIER                     | ID, NOT NULL      | Identifiant unique auto généré.                                          |
| `nom_exploitation`          | CHAÎNE DE CARACTÈRES (250) | NOT NULL          | Nom de famille du comptable.                                             |
| `nom_responsable`           | CHAÎNE DE CARACTÈRES (50)  | NOT NULL          | Nom de famille du comptable.                                             |
| `numero_telephone`          | CHAÎNE DE CARACTÈRES (20)  | NOT NULL          | Numéro de téléphone où on peut joindre le comptable.                     |
| `SAU`                       | REEL                    | NOT NULL          | Surface Agricole Commune, exprimée en are (décamètre carré)              |
| `id_commune_exploitation`   | ENTIER                     | NOT NULL          | Référence la commune où se situe l'exploitation.                         |
| `id_comptable_exploitation` | ENTIER                     | NOT NULL          | Référence le comptable gérant l'exploitation.                            |

