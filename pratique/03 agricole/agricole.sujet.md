# Comptabilité agricole
#### DIFFICULTÉ : 2 / 5

## Contexte
L'agence comptable a besoin d'informatiser ses exploitations et demande à un stagiaire de créer une application pour répondre au besoin.

## Explications
Nous avons en charge la gestion des dossiers comptables d'un centre de gestion pour les exploitations agricoles du département. Un comptable peut gérer plusieurs exploitations, certains d'entre eux ne gèrent qu'une seule exploitation, en effet elle nécessite un temps plein. Les informations sur les comptables sont basiques mais toutes indispensables. Nous avons le nom, le prénom, leur date d'arrivée dans l'agence et leur numéro de poste téléphonique pour les joindre. Chaque comptable travaille dans une et une seule agence. Cette dernière comporte un nom ainsi que le nom de son directeur. Chaque agence est localisée dans une commune. On peut trouver plusieurs agences dans la même commune. On y indiquera seulement le nom de la commune ainsi que son code postal.  
Les exploitations sont situées sur une commune, on peut trouver une exploitation située géographiquement sur plusieurs communes mais nous ne référencerons qu'une seule commune pour situer l'exploitation agricole. Pour l'exploitation agricole, nous conservons le nom du responsable et son numéro de téléphone ainsi que le nom de l'exploitation et sa SAU (Surface Agricole Commune).

## Règles de gestion
`RG01:` On considère que la commune et la ville sont des homonymes.  
`RG02:` On ne gère pas l'historique de la SAU sur l'exploitation.  
`RG03:` La SAU est exprimée en are (unité de surface en décamètre carré).  
`RG04:` Nous avons le référentiel complet des communes du département, il est à penser que de nombreuses communes soient inutiles pour notre gestion mais on préfère garder ce référentiel pour permettre la gestion de nouveaux agriculteurs sur des communes encore non prises en charge.

## Travail
- Réaliser un MCD à partir de l'énoncé ci-dessus.
- Proposer un dictionnaire de données.  
  Il faut indiquer le nom de l'attribut (nom technique), son type et sa taille éventuelle, une description éventuelle. Pour chaque attribut, il faut préciser l'entité.
- Réaliser un MPD à l'aide du MCD précédemment créé et de l'énoncé ci-dessus.

## Livrables attendus
Vos travaux peuvent se représenter via une image (png, jpg), vous enregistrez votre fichier sous cette forme : `[votre_nom]_agricole.mcd.[ext]`.  
Déposez ce fichier dans le partage de Dropbox ou Drive.
