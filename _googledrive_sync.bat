title CMA : Sync vers Google Drive
@echo off

chcp 65001

set file=*.pdf *.png *.sql *.url *.config *.csv *.jpg *.zip *.dump

set src="C:\Users\Guillaume\Data\Learning\CMA\Aborder le SQL"
set destination="C:\Users\Guillaume\Google Drive\CMA\Aborder le SQL"

set excluded_folders=%src%\.vscode %src%\administratif\candidatures %src%\administratif\fiches %src%\administratif\mails  %src%\cours\src %src%\src
set excluded_files=_*

set "robotpath=C:\Program Files (x86)\Windows Resource Kits\Tools"
set "execpath=%robotpath%\robocopy.exe"

echo "%execpath%"


"%execpath%" %src% %destination% %file% /MIR /XD %excluded_folders% /XF %excluded_files%

echo En attente du démarrage de synchronisation vers GoogleDrive...

start "Synchronisation vers Google Drive..." "C:\Program Files\Google\Drive\googledrivesync.exe"

pause