﻿# Aborder et Approfondir le SQL


  - [X] `0.1 Contact`
  - [X] `0.2 Roadmap`
  - [X] `0.3 Vue d'ensemble`
  - [X] `0.4 Propriétés ACID`
  - [X] `1.0 Modéliser`
  - [X] `1.1 Formes Normales`
  - [X] `2.0 Définition des données`
  - [ ] `2.1 Données temporelles`
  - [X] `2.2 Marqueur NULL`
  - [X] `3.0 Prise en main`
  - [X] `3.1 Prise en main avec MySQL`
  - [X] `3.2 Installation de MyCLI`
  - [X] `3.3 Prise en main avec SQL Server`
  - [ ] `3.4 Prise en main avec PostgreSQL`
  - [X] `4.0 Création des objets`
  - [X] `4.1 Conventions`
  - [X] `4.2 Intégrité référentielle`
  - [X] `4.3 Evolution des objets`
  - [X] `5.0 Mise à jour des données`
  - [X] `6.0 Recherches simples`
  - [X] `6.1 Prédicats`
  - [X] `6.2 Algèbre de Boole`
  - [ ] `6.3 Expression non évaluable (UNKNOWN)`
  - [X] `7.0 Algèbre relationnelle`
  - [X] `7.1 Jointures`
  - [ ] `7.2 Pivots`
  - [X] `8.0 Fonctions scalaires`
  - [X] `8.1 Agrégats`
  - [ ] `9.0 Index`
  - [ ] `9.1 Recherche plain-text` 
  - [ ] `10.0 Import-Export`
  - [ ] `11.0 Gestion des droits`
  - [ ] `12.0 Transactions`
  - [ ] `12.1 Curseurs`
  - [ ] `12.2 Procédures Stockées`
  - [ ] `12.3 Déclencheurs`
  - [ ] `12.4 Fonctions et méthodes`
  - [ ] `12.5 Auto-incrément`
  - [ ] `12.6 Lock & Dead Lock`


# Détail cours

---
## `0.1 Contact`
Mes coordonnées pour me joindre. De préférence privilégier Slack pour les discussions. Cela permettra un échange collaboratif entre les auditeurs.

---
## `0.2 Roadmap`
Grandes directives de la formation : Aborder le SQL.

---
## `0.3 Vue d'ensemble`

---
## `0.4 Propriétés ACID`

---
## `1.0 Modéliser`

---
## `1.1 Formes Normales`

---
## `2.0 Définition des données`

---
## `2.2 Marqueur NULL`

---
## `3.0 Prise en main`
Pour exposer le fonctionnement d'un moteur SQL, à quoi ressemble une architecture client/server.

---
## `3.1 Prise en main de MySQL`

---
## `3.2 Installation de MyCLI`

---
## `3.3 Prise en main de SQL Server`

---
## `3.4 Outils`
Pour la modélisation : www.draw.io
Pour les scripts : Présenter des EDI intéressants : VSCode + extensions / VI (Linux) / Notepad++ / SublimeText
POur les interactions avec les SGBDR : Présenter l'utilisation des outils suivants : HeidiSQL, phpMyAdmin, uWamp, xampp & Linux, SSMS, MySQL Workbench.

---
## `4.0 Création des objets`

---
## `4.1 Conventions`
Idées à aborder : Qu'est-ce que la Casse, la Chasse, les caractères diacritiques, les mots clefs, l'indentation, le camelCase, le PascalCase, le snake_case, le SCREAMING_SNAKE_CASE, etc

---
## `4.2 Intégrité référentielle`

---
## `4.3 Evolution des objets`

---
## `5.0 Mise à jour des données`


---
## `6.0 Recherches simples`

---
## `6.1 Algèbre de Boole`

---
## `6.2 Prédicats`

---
## `6.3 Expression non évaluable`
Le traitement du UNKNOWN
cf. Livre Synthèse SQL page 111


---
## `7.0 Algèbre relationnelle`

---
## `7.1 Jointures`

---
## `7.2 Pivots`

---
## `8.0 Fonctions scalaires`

---
## `8.1 Agrégats`

---
## `8.2 Fonctions d'agrégation`

---
## `9.0 Index`

---
## `9.1 Recherche plain-texte`

---
## `10.0 Import/Export CSV`

Il faut se connecter sur le moteur et exécuter la requête en suivant l'exemple ci-dessous.

``` sql
SELECT idDomaine, domaine, idDomaineParent FROM domaines
INTO OUTFILE 'C:/Users/Guillaume/Data/cma_db.domaines.csv'
CHARACTER SET utf8
FIELDS ENCLOSED BY ''
TERMINATED BY ';'
ESCAPED BY ''
LINES TERMINATED BY '\n';
```


---
## `11.0 Gestion des droits`

---
## `12.0 Transactions`

---
## `12.1 Curseurs`

---
## `12.2 Procédures stockées`

---
## `12.3 Déclencheurs`

---
## `12.4 Fonctions et méthodes`

---
## `12.5 Auto-incrément`

---
## `12.6 Lock & Dead Lock`
Utilisation des verrous. On peut voir les verrous de lignes et les verrous de table.




# Idées

- [ ] Avoir des supports disponibles en `direct live` lors des cours. Ce serait bien d'avoir des représentations au format Web, hébergées sur mon blog ou sur une dépôt libre. Si les `slides` pouvaient être responsives et pouvant s'adapter aux conditions des salles (luminosité) avec des thèmes ou une Typographie adaptée.

- [ ] Des présentations Web entraînent des supports lisibles ou imprimables (PDF). Faudrait gérer la documentation au format `Markdown` ou `LateX` pour l'inscrire dans un gestionnaire de code ou même pourquoi pas un GED gratuit.

- [x] Automatiser la synchronisation vers le dépôt `DropBox` en filtrant les types de fichiers disponibles et ceux restant privés (.pptx, src, md, xml, drawio, etc).

- [ ] Proposer des TP d'exercices (plastifiés) pour offrir une approche conviviale des exercices lors des cours.

- [ ] Alimenter le blog pour être visible et inscrire des notes plus pertinentes que de simples `slides` qui exposent une histoire.

- [ ] Trouver une solution pour permettre une base de données commune pour tous. Des auditeurs qui se connecteraient sur un serveur Web pour faire des requêtes et ainsi alimenter en base des données. Faudrait également automatiser des sauvegardes des données inscrites pour ne pas voir un auditeurs tout casser avec un malheureux `DELETE` sans `WHERE`...

- [ ] Focus sur la bibliothèque, employees et aviation pour les exemples.

- [ ] Proposer des exercices sur `Postgresql`.

- [ ] Se mettre à `Linux`.

- [ ] Distinguer les exemples des exercices dans le support pédagogique. Les bases de données utilisées à titre d'exemple devraient se distinguer des bases de données liées aux exercices. Cela implique un nettoyage des espaces exercices.



# Exemples et exercices

Afin d'assimiler au mieux tous les aspects pédagogiques, l'ensemble des cours est étayé d'exemples. Pour consolider les connaissances, des exercices sont mis à disposition et doivent se décorrélés des exemples.

## Supports pour les exemples
 - [ ] MCD, MLD, MPD `bibliotheque`
 - [ ] Création de structures (bdd, tables, contraintes, bonus:auto-increment) `bibliotheque`
 - [ ] Mise à jour de données `adhesions`
 - [ ] Evolution de structures (alter, drop) `???`
 - [ ] Création d'objets satellites (view, store procedure, trigger, index) `???`
 - [ ] Importation/Exportation de données volumineuses (csv) `bibliotheque panama_papers adventure_works`
 - [ ] Gestion des types de données volumineux (blob, clob, images, json) `???`
 - [ ] Recherches simples (projection, restriction et tri) `???`
 - [ ] Jointures et division `domaines_professionnels`
 - [ ] Opérations ensemblistes (union, intersection, différence et produit cartésien) `???`
 - [ ] Fonctions (scalaires) `???`
 - [ ] Agrégats `???`
 - [ ] Recherches plain-text `???`
 - [ ] Gestion des droits (DCL : Data Control Language - revoke & grant) `???`
 - [ ] Lock & Dead Lock `???`
 - [ ] Reverse Engineering ou rétro-ingénierie (db > models) `bibliotheque`


## Supports pour les exercices
 - [ ] MCD, MLD, MPD `cma security agricole attribution evaluation`
 - [ ] Création de structures (bdd, tables, contraintes, bonus:auto-increment) `cma security`
 - [ ] Mise à jour de données `security`
 - [ ] Evolution de structures (alter, drop) `cma attribution`
 - [ ] Création d'objets satellites (view, store procedure, trigger, index) `TODO`
 - [ ] Importation/Exportation de données volumineuses (csv) `globe`
 - [ ] Gestion des types de données volumineux (blob, clob, images, json) `artoile`
 - [ ] Recherches simples (projection, restriction et tri) ``
 - [ ] Jointures et division `aviation`
 - [ ] Opérations ensemblistes (union, intersection, différence et produit cartésien) `TODO`
 - [ ] Fonctions (scalaires) `TODO`
 - [ ] Agrégats `panama_papers`
 - [ ] Recherches plain-text ``
 - [ ] Gestion des droits (DCL : Data Control Language - revoke & grant) ``
 - [ ] Lock & Dead Lock ``
 - [ ] Reverse Engineering ou rétro-ingénierie (db > models) `adventure_works`
 

## Bases de données disponibles :
`attribution` exercice pour modéliser et faire évoluer d'un besoin.
`cma`
`inscription` exercice pour modéliser une bdd à partir d'un fichier Excel.
`artoile` exemple pour insérer une image.
`aviation` exercice sur la division relationnelle
`scolaire` exercice sur l'algèbre de Boole (expressions et connecteurs logiques)
`domaines_professionnels` exemple pour les jointures
`adhesions` exemples pour les mises à jour des données.
`mouvements_litteraires` exercices sur les mises à jour de données.
`acces` exercice
`profils` exercice
`personnes` exemple pour les recherches simples et les jointures et opérations ensemblistes.
`compoman` exercice
`ventes` exercice de modélisation à partir d'un dossier contenant des supports divers (csv, document, règles de gestion).
`evaluation` exercice pédagogique lié à la lecture d'un fichier Excel existant.
`disquaire` exercice
`atomes` création d'une simple table avec un INSERT avec nombreux pays.
`departements` création d'une simple table avec un INSERT avec nombreux pays.
`matrice_appels` exercice de modélisation à partir de maquettes uniquement.
`globe` création d'une simple table avec un INSERT avec nombreux pays.
`agricole` exercice de modélisation.
`bibliotheque` exemple complet (mcd, mld, dico, mpd, insert, csv, requêtes).
`email` création d'une simple table avec un INSERT avec nombreux pays.
`security` exercice de modélisation complet et de création de structure de bdd.
`santa_workshop`
`employees` exemple d'importation via des commandes d'importation de fichiers dump en MySQL.
`adventure_works` exercice pour du reverse engineering (à partir de csv et scripts SQL).
`keywords` exercice pour les conventions de nommages des objets en SQL.
`panama_papers` exercice pour des agrégats de données.



# Exemples

 - [ ] Requêtes utiles (show databases, ...)
 - [ ] Commentaires
 - [ ] Divers types de données
 - [ ] Marqueur NULL
 - [ ] Type Domaine
 - [ ] Charset & Collation
 - [ ] Create database
 - [ ] Create schema
 - [ ] Create table
 - [ ] Create view
 - [ ] Create store procedure
 - [ ] Create trigger
 - [ ] Create index
 - [ ] Constraint Check
 - [ ] Constraint Default
 - [ ] Constraint Not Null
 - [ ] Constraint Unique
 - [ ] Constraint Primary Key
 - [ ] Constraint Foreign Key
 - [ ] Constraint Foreign Key with Clauses (null, default, cascade)
 - [ ] Constraint Foreign Key with MATCH (full, partial, simple)
 - [ ] MAJ Insert 
 - [ ] MAJ Update
 - [ ] MAJ Delete
 - [ ] MAJ Merge
 - [ ] WHERE connecteurs logiques
 - [ ] WHERE marqeur NULL
 - [ ] WHERE  opérateurs
 - [ ] SELECT projection
 - [ ] WHERE restriction
 - [ ] ORDER BY
 - [ ] Cross Join
 - [ ] Except
 - [ ] Natural Join
 - [ ] Inner Join
 - [ ] Outer Join
 - [ ] Old school
 - [ ] Union
 - [ ] Intersect